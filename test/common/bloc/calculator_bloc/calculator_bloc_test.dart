import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simple_calculator/common/bloc/calculator_bloc/bloc.dart';
import 'package:simple_calculator/common/injector/injector.dart';
import 'package:simple_calculator/domain/entities/calculator_item_entity.dart';
import 'package:simple_calculator/domain/usecases/__mocks__/calculator_usecase_test.dart';
import 'package:simple_calculator/domain/usecases/calclulator_usecase.dart';

void main() {
  CalculatorUsecase calculatorUsecase;
  CalculatorBloc calculatorBloc;

  setUp(() {
    Injector.container
        .registerInstance<CalculatorUsecase>(MockCalculatorUsecase());

    calculatorUsecase = Injector.resolve<CalculatorUsecase>();
    calculatorBloc = CalculatorBloc(
      calculatorUsecase: calculatorUsecase,
    );
  });

  tearDown(() {
    calculatorBloc?.close();
    Injector.container?.clear();
  });
  group('Calculator Bloc ', () {
    blocTest(
        'should trigger state [CalculatorResultState] '
        'when user call event [CalculatorAddNumberEvent]',
        build: () => calculatorBloc,
        act: (bloc) async {
          const int number = 1;
          bloc.add(CalculatorAddNumberEvent(number));
        },
        verify: (bloc) {
          expect(bloc.state, isA<CalculatorResultState>());
          expect(bloc.state.result, '1.0');
        },
        expect: <dynamic>[
          isA<CalculatorResultState>(),
        ]);
    blocTest(
        'should trigger state [CalculatorResultState] '
        'when user call event [CalculatorAddNumberEvent] with coma result',
        build: () => calculatorBloc,
        act: (bloc) async {
          const int number = 1;
          const int number2 = 2;
          bloc.add(CalculatorAddNumberEvent(number));
          bloc.add(CalculatorOperateEvent(Operator.division));
          bloc.add(CalculatorAddNumberEvent(number2));
          bloc.add(CalculatorCountEvent());
          bloc.add(CalculatorAddNumberEvent(number));
        },
        verify: (bloc) {
          expect(bloc.state, isA<CalculatorResultState>());
          expect(bloc.state.result, '1.5');
        },
        expect: <dynamic>[
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
        ]);

    blocTest(
        'should trigger state [CalculatorResultState] '
        'when user call event [CalculatorOperateEvent]',
        build: () => calculatorBloc,
        act: (bloc) async {
          bloc.add(CalculatorOperateEvent(Operator.addition));
        },
        verify: (bloc) {
          expect(bloc.state.result, '0');
          expect(bloc.state.prevOperation, Operator.addition);
        },
        expect: <dynamic>[
          isA<CalculatorResultState>(),
        ]);

    blocTest(
        'should trigger state [CalculatorResultState] '
        'when user call event [CalculatorAddNumberEvent] '
        'then will calculate when prevResult is not empty',
        build: () => calculatorBloc,
        act: (bloc) async {
          bloc.add(CalculatorAddNumberEvent(3));
          bloc.add(CalculatorOperateEvent(Operator.substract));
          bloc.add(CalculatorAddNumberEvent(1));
          bloc.add(CalculatorOperateEvent(Operator.addition));
        },
        verify: (bloc) {
          expect(bloc.state, isA<CalculatorResultState>());
          expect(bloc.state.prevResult, '2.0');
        },
        expect: <dynamic>[
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
        ]);
    blocTest(
        'should trigger state [CalculatorResultState] '
        'when user call event [CalculatorCountEvent] '
        'then will calculate and store result to local data',
        build: () => calculatorBloc,
        act: (bloc) async {
          bloc.add(CalculatorAddNumberEvent(3));
          bloc.add(CalculatorOperateEvent(Operator.substract));
          bloc.add(CalculatorAddNumberEvent(1));
          bloc.add(CalculatorCountEvent());
        },
        verify: (bloc) {
          expect(bloc.state, isA<CalculatorResultState>());
          expect(bloc.state.result, '2.0');
        },
        expect: <dynamic>[
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
        ]);

    blocTest(
        'should trigger state [CalculatorResultState] '
        'when user call event [CalculatorCountEvent] '
        'then will set result to 0 when no operation',
        build: () => calculatorBloc,
        act: (bloc) async {
          bloc.add(CalculatorCountEvent());
        },
        verify: (bloc) {
          expect(bloc.state, isA<CalculatorResultState>());
          expect(bloc.state.result, '0');
        },
        expect: <dynamic>[
          isA<CalculatorResultState>(),
        ]);
    blocTest(
        'should trigger state [CalculatorResultState] '
        'when user call event [CalculatorEraseEvent] '
        'then will set result to 123 when result just is 1234',
        build: () => calculatorBloc,
        act: (bloc) async {
          bloc.add(CalculatorAddNumberEvent(1234));
          bloc.add(CalculatorEraseEvent());
        },
        verify: (bloc) {
          expect(bloc.state, isA<CalculatorResultState>());
          expect(bloc.state.result, '123.0');
        },
        expect: <dynamic>[
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
        ]);

    blocTest(
        'should trigger state [CalculatorResultState] '
        'when user call event [CalculatorEraseEvent] '
        'then will set result to 7 when result just is 7.5',
        build: () => calculatorBloc,
        act: (bloc) async {
          bloc.add(CalculatorAddNumberEvent(15));
          bloc.add(CalculatorOperateEvent(Operator.division));
          bloc.add(CalculatorAddNumberEvent(2));
          bloc.add(CalculatorCountEvent());
          bloc.add(CalculatorEraseEvent());
        },
        verify: (bloc) {
          expect(bloc.state, isA<CalculatorResultState>());
          expect(bloc.state.result, '7.0');
        },
        expect: <dynamic>[
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
        ]);

    blocTest(
        'should trigger state [CalculatorResultState] '
        'when user call event [CalculatorEraseEvent] '
        'then will set result to 0 when result just is 7',
        build: () => calculatorBloc,
        act: (bloc) async {
          bloc.add(CalculatorAddNumberEvent(7));
          bloc.add(CalculatorCountEvent());
          bloc.add(CalculatorEraseEvent());
        },
        verify: (bloc) {
          expect(bloc.state, isA<CalculatorResultState>());
          expect(bloc.state.result, '0');
        },
        expect: <dynamic>[
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
        ]);
    blocTest(
        'should trigger state [CalculatorInitState] '
        'when user call event [CalculatorCleanEvent] ',
        build: () => calculatorBloc,
        act: (bloc) async {
          bloc.add(CalculatorCleanEvent());
        },
        verify: (bloc) {
          expect(bloc.state, isA<CalculatorInitState>());
          expect(bloc.state.result, '0');
          expect(bloc.state.prevResult, isEmpty);
          expect(bloc.state.prevOperation, isNull);
        },
        expect: <dynamic>[
          isA<CalculatorInitState>(),
        ]);
    blocTest(
        'should trigger state [CalculatorInitState] '
        'when user call event [CalculatorCountEvent] '
        'then will addition a to b',
        build: () => calculatorBloc,
        act: (bloc) async {
          bloc.add(CalculatorAddNumberEvent(1));
          bloc.add(CalculatorOperateEvent(Operator.addition));
          bloc.add(CalculatorAddNumberEvent(2));
          bloc.add(CalculatorCountEvent());
        },
        verify: (bloc) {
          expect(bloc.state, isA<CalculatorResultState>());
          expect(bloc.state.result, '3.0');
          expect(bloc.state.prevResult, isEmpty);
          expect(bloc.state.prevOperation, isNull);
        },
        expect: <dynamic>[
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
        ]);
    blocTest(
        'should trigger state [CalculatorInitState] '
        'when user call event [CalculatorCountEvent] '
        'then will divide a with b',
        build: () => calculatorBloc,
        act: (bloc) async {
          bloc.add(CalculatorAddNumberEvent(1));
          bloc.add(CalculatorOperateEvent(Operator.division));
          bloc.add(CalculatorAddNumberEvent(2));
          bloc.add(CalculatorCountEvent());
        },
        verify: (bloc) {
          expect(bloc.state, isA<CalculatorResultState>());
          expect(bloc.state.result, '0.5');
          expect(bloc.state.prevResult, isEmpty);
          expect(bloc.state.prevOperation, isNull);
        },
        expect: <dynamic>[
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
        ]);
    blocTest(
        'should trigger state [CalculatorInitState] '
        'when user call event [CalculatorCountEvent] '
        'then will multiply a with b',
        build: () => calculatorBloc,
        act: (bloc) async {
          bloc.add(CalculatorAddNumberEvent(1));
          bloc.add(CalculatorOperateEvent(Operator.multiply));
          bloc.add(CalculatorAddNumberEvent(2));
          bloc.add(CalculatorCountEvent());
        },
        verify: (bloc) {
          expect(bloc.state, isA<CalculatorResultState>());
          expect(bloc.state.result, '2.0');
          expect(bloc.state.prevResult, isEmpty);
          expect(bloc.state.prevOperation, isNull);
        },
        expect: <dynamic>[
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
          isA<CalculatorResultState>(),
        ]);
  });
}
