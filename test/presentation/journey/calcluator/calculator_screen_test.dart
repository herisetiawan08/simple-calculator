import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:simple_calculator/common/bloc/calculator_bloc/__mocks__/calculator_bloc_mock.dart';
import 'package:simple_calculator/common/bloc/calculator_bloc/bloc.dart';
import 'package:simple_calculator/common/constants/calculator_constants.dart';
import 'package:simple_calculator/common/injector/injector.dart';
import 'package:simple_calculator/domain/entities/calculator_history_entity.dart';
import 'package:simple_calculator/domain/usecases/__mocks__/calculator_usecase_test.dart';
import 'package:simple_calculator/domain/usecases/calclulator_usecase.dart';
import 'package:simple_calculator/presentation/journey/calculator/calculator_screen.dart';
import 'package:simple_calculator/presentation/widgets/row_builder.dart';

void main() {
  CalculatorBloc calculatorBloc;

  setUp(() {
    Injector.container
        .registerInstance<CalculatorUsecase>(MockCalculatorUsecase());
    Injector.container.registerInstance<CalculatorBloc>(MockCalculatorBloc());
    calculatorBloc = Injector.resolve<CalculatorBloc>();
  });

  tearDown(() {
    calculatorBloc?.close();
    Injector.container?.clear();
  });

  void setUpScreenTest(WidgetTester tester, Size size) {
    tester.binding.window.physicalSizeTestValue = size;
    tester.binding.window.devicePixelRatioTestValue = 1.0;
    addTearDown(tester.binding.window.clearPhysicalSizeTestValue);
  }

  group('Calculator Screen Test ', () {
    testWidgets('should render screen correctly', (WidgetTester tester) async {
      when(Injector.resolve<CalculatorUsecase>().getAllHistories())
          .thenAnswer((_) async => []);
      when(calculatorBloc.state).thenAnswer((_) => CalculatorInitState());
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: CalculatorScreen(
              calculatorBloc: calculatorBloc,
            ),
          ),
        ),
      );

      expect(
        find.byKey(CalculatorConstants.oneKey),
        findsOneWidget,
      );
      expect(
        find.byKey(CalculatorConstants.twoKey),
        findsOneWidget,
      );
      expect(
        find.byKey(CalculatorConstants.threeKey),
        findsOneWidget,
      );
      expect(
        find.byKey(CalculatorConstants.fourKey),
        findsOneWidget,
      );
      expect(
        find.byKey(CalculatorConstants.fiveKey),
        findsOneWidget,
      );
      expect(
        find.byKey(CalculatorConstants.sixKey),
        findsOneWidget,
      );

      verify(Injector.resolve<CalculatorUsecase>().getAllHistories()).called(1);
    });

    testWidgets(
        'should tap on [oneKey] and calling [CalculatorAddNumberEvent] ',
        (WidgetTester tester) async {
      when(Injector.resolve<CalculatorUsecase>().getAllHistories())
          .thenAnswer((_) async => []);
      when(calculatorBloc.state).thenAnswer((_) => CalculatorInitState());
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: CalculatorScreen(
              calculatorBloc: calculatorBloc,
            ),
          ),
        ),
      );

      await tester.tap(find.byKey(CalculatorConstants.oneKey));

      await tester.pumpAndSettle();

      verify(calculatorBloc.add(CalculatorAddNumberEvent(1))).called(1);
    });

    testWidgets(
        'should call [CalculatorCountEvent] '
        'when [item.operation] is [Operator.result]',
        (WidgetTester tester) async {
      when(Injector.resolve<CalculatorUsecase>().getAllHistories())
          .thenAnswer((_) async => []);
      when(calculatorBloc.state).thenAnswer((_) => CalculatorInitState());
      setUpScreenTest(tester, Size(720, 1280));
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: CalculatorScreen(
              calculatorBloc: calculatorBloc,
            ),
          ),
        ),
      );

      expect(find.byKey(CalculatorConstants.resultKey), findsOneWidget);

      await tester.tap(find.byKey(CalculatorConstants.resultKey));

      await tester.pumpAndSettle();

      verify(calculatorBloc.add(any)).called(1);
    });

    testWidgets(
        'should call [CalculatorEraseEvent] '
        'when [item.operation] is [Operator.erase]',
        (WidgetTester tester) async {
      when(Injector.resolve<CalculatorUsecase>().getAllHistories())
          .thenAnswer((_) async => []);
      when(calculatorBloc.state).thenAnswer((_) => CalculatorInitState());
      setUpScreenTest(tester, Size(720, 1280));
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: CalculatorScreen(
              calculatorBloc: calculatorBloc,
            ),
          ),
        ),
      );

      expect(find.byKey(CalculatorConstants.deleteKey), findsOneWidget);

      await tester.tap(find.byKey(CalculatorConstants.deleteKey));

      await tester.pumpAndSettle();

      verify(calculatorBloc.add(any)).called(1);
    });

    testWidgets(
        'should call [CalculatorCleanEvent] '
        'when [item.operation] is [Operator.clean]',
        (WidgetTester tester) async {
      when(Injector.resolve<CalculatorUsecase>().getAllHistories())
          .thenAnswer((_) async => []);
      when(calculatorBloc.state).thenAnswer((_) => CalculatorInitState());
      setUpScreenTest(tester, Size(720, 1280));
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: CalculatorScreen(
              calculatorBloc: calculatorBloc,
            ),
          ),
        ),
      );

      expect(find.byKey(CalculatorConstants.cleanKey), findsOneWidget);

      await tester.tap(find.byKey(CalculatorConstants.cleanKey));

      await tester.pumpAndSettle();

      verify(calculatorBloc.add(any)).called(1);
    });

    testWidgets(
        'should call [CalculatorOperateEvent] '
        'when [item.operation] is [Operator.addition] or other',
        (WidgetTester tester) async {
      when(Injector.resolve<CalculatorUsecase>().getAllHistories())
          .thenAnswer((_) async => []);
      when(calculatorBloc.state).thenAnswer((_) => CalculatorInitState());
      setUpScreenTest(tester, Size(720, 1280));
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: CalculatorScreen(
              calculatorBloc: calculatorBloc,
            ),
          ),
        ),
      );

      expect(find.byKey(CalculatorConstants.additionKey), findsOneWidget);

      await tester.tap(find.byKey(CalculatorConstants.additionKey));

      await tester.pumpAndSettle();

      verify(calculatorBloc.add(any)).called(1);
    });

    testWidgets('should call [cleanAllHistory] when user tap on [IconButton]',
        (WidgetTester tester) async {
      when(Injector.resolve<CalculatorUsecase>().getAllHistories())
          .thenAnswer((_) async => []);
      when(Injector.resolve<CalculatorUsecase>().cleanAllHistory())
          .thenAnswer((_) async => {});
      when(calculatorBloc.state).thenAnswer((_) => CalculatorInitState());
      setUpScreenTest(tester, Size(720, 1280));
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: CalculatorScreen(
              calculatorBloc: calculatorBloc,
            ),
          ),
        ),
      );

      expect(find.byType(IconButton), findsOneWidget);

      await tester.tap(find.byType(IconButton));

      await tester.pumpAndSettle();

      verify(Injector.resolve<CalculatorUsecase>().cleanAllHistory()).called(1);
    });

    testWidgets('sould render history of operation',
        (WidgetTester tester) async {
      const String expected = '1+2=3';
      when(Injector.resolve<CalculatorUsecase>().getAllHistories())
          .thenAnswer((_) async => [
                CalculatorHistoryEntity(
                  firstNumber: '1',
                  secondNumber: '2',
                  operation: 'addition',
                  result: '3',
                  timestamp: DateTime.now().toIso8601String(),
                )
              ]);
      when(Injector.resolve<CalculatorUsecase>().cleanAllHistory())
          .thenAnswer((_) async => {});
      when(calculatorBloc.state).thenAnswer((_) => CalculatorInitState());
      setUpScreenTest(tester, Size(720, 1280));
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: CalculatorScreen(
              calculatorBloc: calculatorBloc,
            ),
          ),
        ),
      );

      await tester.pumpAndSettle();

      expect(find.text(expected), findsOneWidget);
    });

    testWidgets(
        'sould render history of operation '
        'then delete history when user tap history text',
        (WidgetTester tester) async {
      const String expected = '1+2=3';
      when(Injector.resolve<CalculatorUsecase>().getAllHistories())
          .thenAnswer((_) async => [
                CalculatorHistoryEntity(
                  firstNumber: '1',
                  secondNumber: '2',
                  operation: 'addition',
                  result: '3',
                  timestamp: DateTime.now().toIso8601String(),
                )
              ]);
      when(Injector.resolve<CalculatorUsecase>().cleanAllHistory())
          .thenAnswer((_) async => {});
      when(Injector.resolve<CalculatorUsecase>().deleteHistory(any))
          .thenAnswer((_) async => {});
      when(calculatorBloc.state).thenAnswer((_) => CalculatorInitState());
      setUpScreenTest(tester, Size(720, 1280));
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: CalculatorScreen(
              calculatorBloc: calculatorBloc,
            ),
          ),
        ),
      );

      await tester.pumpAndSettle();

      expect(find.text(expected), findsOneWidget);

      await tester.tap(find.text(expected));

      verify(Injector.resolve<CalculatorUsecase>().deleteHistory(any))
          .called(1);
    });

    testWidgets('should call callback when function called',
        (WidgetTester tester) async {
      when(Injector.resolve<CalculatorUsecase>().getAllHistories())
          .thenAnswer((_) async => [
                CalculatorHistoryEntity(
                  firstNumber: '1',
                  secondNumber: '2',
                  operation: 'addition',
                  result: '3',
                  timestamp: DateTime.now().toIso8601String(),
                )
              ]);
      when(Injector.resolve<CalculatorUsecase>().cleanAllHistory())
          .thenAnswer((_) async => {});
      when(Injector.resolve<CalculatorUsecase>().deleteHistory(any))
          .thenAnswer((_) async => {});
      when(calculatorBloc.state).thenAnswer((_) => CalculatorInitState());
      setUpScreenTest(tester, Size(720, 1280));
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: CalculatorScreen(
              calculatorBloc: calculatorBloc,
            ),
          ),
        ),
      );

      await tester.pumpAndSettle();

      final rowBuilderFinder = find.byType(RowBuilder);

      final RowBuilder rowBuilder = rowBuilderFinder.evaluate().first.widget;

      rowBuilder?.resultCallback();

      await tester.pumpAndSettle();

      verify(Injector.resolve<CalculatorUsecase>().getAllHistories()).called(2);
    });
  });
}
