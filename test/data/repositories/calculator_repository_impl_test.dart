import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:simple_calculator/common/injector/injector.dart';
import 'package:simple_calculator/data/datasource/local/__mocks__/calculator_history_local_datasource_mock.dart';
import 'package:simple_calculator/data/datasource/local/calculator_history_local_datasource.dart';
import 'package:simple_calculator/data/datasource/local/databases/tables/calculator_history_table.dart';
import 'package:simple_calculator/data/repositories/calculator_repository_impl.dart';
import 'package:simple_calculator/domain/repositories/calculator_repository.dart';

void main() {
  CalculatorHistoryLocalDataSource calculatorHistoryLocalDataSource;
  CalculatorRepository calculatorRepository;

  setUp(() {
    Injector.container.registerInstance<CalculatorHistoryLocalDataSource>(
        MockCalculatorHistoryLocalDataSource());
    calculatorHistoryLocalDataSource =
        Injector.resolve<CalculatorHistoryLocalDataSource>();
    calculatorRepository = CalculatorRepositoryImpl(
        calculatorHistoryLocalDataSource: calculatorHistoryLocalDataSource);
  });

  tearDown(() {
    Injector.container?.clear();
  });
  group('Calculator Repository ', () {
    test('should store history when called function [storeHistoryToLocal]',
        () async {
      final entity = CalculatorHistoryTable();

      when(calculatorHistoryLocalDataSource.put(any, any))
          .thenAnswer((_) async {});
      await calculatorRepository.storeHistoryToLocal(entity);

      verify(calculatorHistoryLocalDataSource.put(any, any)).called(1);
    });
    test('should get all history when called function [getAllHistories]',
        () async {
      when(calculatorHistoryLocalDataSource.getAll())
          .thenAnswer((_) async => []);
      await calculatorRepository.getAllHistories();

      verify(calculatorHistoryLocalDataSource.getAll()).called(1);
    });
    test('should delete history when called function [deleteHistory]',
        () async {
      const String key = 'key';

      when(calculatorHistoryLocalDataSource.detele(key))
          .thenAnswer((_) async {});
      await calculatorRepository.deleteHistory(key);

      verify(calculatorHistoryLocalDataSource.detele(key)).called(1);
    });
    test('should delete all histories when called function [cleanAllHistory]',
        () async {
      when(calculatorHistoryLocalDataSource.deteleAll())
          .thenAnswer((_) async {});
      await calculatorRepository.cleanAllHistory();

      verify(calculatorHistoryLocalDataSource.deteleAll()).called(1);
    });
  });
}
