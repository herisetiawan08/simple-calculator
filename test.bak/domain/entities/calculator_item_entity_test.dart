import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:simple_calculator/domain/entities/calculator_item_entity.dart';

void main() {
  group('CalculatorItem ', () {
    test('should store correctly', () {
      // Given
      const key = ValueKey('test_key');
      const title = 'title';

      //When
      final item = CalculatorEntity(
        key: key,
        title: title,
      );

      //Then
      expect(item.key, key);
      expect(item.title, title);
      expect(item.flex, 1);
    });

    test('should throw assert error when passing both value and operation', () {
      // Given
      const key = ValueKey('test_key');
      const title = 'title';
      dynamic error;

      //When
      try {
        CalculatorEntity(
          key: key,
          title: title,
          value: 1,
          operation: Operator.addition,
        );
      } catch (e) {
        error = e;
      }

      //Then
      expect(error, isA<AssertionError>());
    });
    test('should throw assert error when passing both title and icon', () {
      // Given
      const key = ValueKey('test_key');
      const title = 'title';
      dynamic error;

      //When
      try {
        CalculatorEntity(
          key: key,
          title: title,
          icon: Icons.dangerous,
        );
      } catch (e) {
        error = e;
      }

      //Then
      expect(error, isA<AssertionError>());
    });
  });
}
