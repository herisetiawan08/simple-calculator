import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:simple_calculator/common/bloc/calculator_bloc/__mocks__/calculator_bloc_mock.dart';
import 'package:simple_calculator/common/bloc/calculator_bloc/bloc.dart';
import 'package:simple_calculator/common/constants/calculator_constants.dart';
import 'package:simple_calculator/common/injector/injector.dart';
import 'package:simple_calculator/presentation/journey/calculator/calculator_screen.dart';

void main() {
  CalculatorBloc calculatorBloc;

  setUp(() {
    Injector.container.registerInstance<CalculatorBloc>(MockCalculatorBloc());
    calculatorBloc = Injector.resolve<CalculatorBloc>();
  });

  tearDown(() {
    calculatorBloc?.close();
    Injector.container?.clear();
  });

  void setUpScreenTest(WidgetTester tester, Size size) {
    tester.binding.window.physicalSizeTestValue = size;
    tester.binding.window.devicePixelRatioTestValue = 1.0;
    addTearDown(tester.binding.window.clearPhysicalSizeTestValue);
  }

  group('CalculatorScreen test', () {
    testWidgets('should render screen correctly', (WidgetTester tester) async {
      setUpScreenTest(tester, const Size(720, 1280));
      when(calculatorBloc.state).thenAnswer((_) => CalculatorInitState());
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: CalculatorScreen(
              calculatorBloc: calculatorBloc,
            ),
          ),
        ),
      );

      expect(find.byKey(CalculatorConstants.oneKey), findsOneWidget);
    });

    testWidgets(
        'should render screen correctly '
        'then call event [CalculatorAddNumberEvent] '
        'when user tap on number button', (WidgetTester tester) async {
      setUpScreenTest(tester, const Size(720, 1280));

      when(calculatorBloc.state).thenAnswer((_) => CalculatorInitState());
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: CalculatorScreen(
              calculatorBloc: calculatorBloc,
            ),
          ),
        ),
      );

      expect(find.byKey(CalculatorConstants.oneKey), findsOneWidget);
      await tester.tap(find.byKey(CalculatorConstants.oneKey));
      verify(calculatorBloc.add(any)).called(1);
    });
    testWidgets(
        'should render screen correctly '
        'then call event [CalculatorOperateEvent] '
        'when user tap on operator button', (WidgetTester tester) async {
      setUpScreenTest(tester, const Size(720, 1280));

      when(calculatorBloc.state).thenAnswer((_) => CalculatorInitState());
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: CalculatorScreen(
              calculatorBloc: calculatorBloc,
            ),
          ),
        ),
      );

      expect(find.byKey(CalculatorConstants.additionKey), findsOneWidget);
      await tester.tap(find.byKey(CalculatorConstants.additionKey));
      verify(calculatorBloc.add(any)).called(1);
    });
    testWidgets(
        'should render screen correctly '
        'then call event [CalculatorCountEvent] '
        'when user tap on result button', (WidgetTester tester) async {
      setUpScreenTest(tester, const Size(720, 1280));

      when(calculatorBloc.state).thenAnswer((_) => CalculatorInitState());
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: CalculatorScreen(
              calculatorBloc: calculatorBloc,
            ),
          ),
        ),
      );

      expect(find.byKey(CalculatorConstants.resultKey), findsOneWidget);
      await tester.tap(find.byKey(CalculatorConstants.resultKey));
      verify(calculatorBloc.add(any)).called(1);
    });
    testWidgets(
        'should render screen correctly '
        'then call event [CalculatorEraseEvent] '
        'when user tap on erase button', (WidgetTester tester) async {
      setUpScreenTest(tester, const Size(720, 1280));

      when(calculatorBloc.state).thenAnswer((_) => CalculatorInitState());
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: CalculatorScreen(
              calculatorBloc: calculatorBloc,
            ),
          ),
        ),
      );

      expect(find.byKey(CalculatorConstants.deleteKey), findsOneWidget);
      await tester.tap(find.byKey(CalculatorConstants.deleteKey));
      verify(calculatorBloc.add(any)).called(1);
    });
    testWidgets(
        'should render screen correctly '
        'then call event [CalculatorCleanEvent] '
        'when user tap on AC button', (WidgetTester tester) async {
      setUpScreenTest(tester, const Size(720, 1280));

      when(calculatorBloc.state).thenAnswer((_) => CalculatorInitState());
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: CalculatorScreen(
              calculatorBloc: calculatorBloc,
            ),
          ),
        ),
      );

      expect(find.byKey(CalculatorConstants.cleanKey), findsOneWidget);
      await tester.tap(find.byKey(CalculatorConstants.cleanKey));
      verify(calculatorBloc.add(any)).called(1);
    });
  });
}
