import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simple_calculator/common/bloc/calculator_bloc/bloc.dart';
import 'package:simple_calculator/common/injector/injector.dart';
import 'package:simple_calculator/domain/entities/calculator_history_entity.dart';
import 'package:simple_calculator/domain/entities/calculator_item_entity.dart';
import 'package:simple_calculator/domain/usecases/calclulator_usecase.dart';
import 'package:simple_calculator/presentation/widgets/row_builder.dart';

class ColumnBuilder extends StatefulWidget {
  final List<List<CalculatorEntity>> calcItems;

  ColumnBuilder({this.calcItems});

  @override
  _ColumnBuilderState createState() => _ColumnBuilderState();
}

class _ColumnBuilderState extends State<ColumnBuilder> {
  List<CalculatorHistoryEntity> histories = [];
  final ScrollController scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    getHistories();
  }

  void getHistories() async {
    final _histories =
        await Injector.resolve<CalculatorUsecase>().getAllHistories();
    setState(() {
      histories = _histories
        ..sort((a, b) => b.timestamp.compareTo(a.timestamp));
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      controller: scrollController,
      reverse: true,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: EdgeInsets.only(right: 10),
            width: MediaQuery.of(context).size.width,
            child: BlocBuilder<CalculatorBloc, CalculatorState>(
              builder: (context, state) => Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Row(
                    children: [
                      IconButton(
                          icon: Icon(Icons.delete_forever),
                          onPressed: () {
                            Injector.resolve<CalculatorUsecase>()
                                .cleanAllHistory();
                            getHistories();
                          }),
                    ],
                  ),
                  ...histories.map(
                    (history) => GestureDetector(
                      onTap: () {
                        Injector.resolve<CalculatorUsecase>()
                            .deleteHistory(history.timestamp);
                        getHistories();
                      },
                      child: Text(
                        history.firstNumber +
                            _buildOperatorSymbol(
                                history.operation.toOperation()) +
                            history.secondNumber +
                            '=' +
                            history.result,
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                  ),
                  Text(
                    state.prevResult,
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.grey,
                    ),
                  ),
                  Text(
                    _buildOperatorSymbol(state.prevOperation) + state.result,
                    textAlign: TextAlign.end,
                    style: TextStyle(
                      fontSize: 40,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
            ),
          ),
          // GestureDetector(),
          ...widget.calcItems
              .map((item) => RowBuilder(
                  items: item,
                  resultCallback: () {
                    getHistories();
                  }))
              .toList(),
        ],
      ),
    );
  }

  String _buildOperatorSymbol(Operator operation) {
    switch (operation) {
      case Operator.addition:
        return '+';
      case Operator.substract:
        return '-';
      case Operator.multiply:
        return 'x';
      case Operator.division:
        return '÷';
      default:
        return '';
    }
  }
}
