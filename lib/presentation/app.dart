import 'package:flutter/material.dart';
import 'package:simple_calculator/common/bloc/calculator_bloc/bloc.dart';
import 'package:simple_calculator/common/injector/injector.dart';
import 'package:simple_calculator/presentation/journey/calculator/calculator_screen.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Simple Calculator'),
        ),
        body: CalculatorScreen(
          calculatorBloc: Injector.resolve<CalculatorBloc>(),
        ),
      ),
    );
  }
}
