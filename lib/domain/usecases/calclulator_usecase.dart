import 'package:simple_calculator/domain/entities/calculator_history_entity.dart';
import 'package:simple_calculator/domain/repositories/calculator_repository.dart';

class CalculatorUsecase {
  CalculatorRepository calculatorRepository;
  CalculatorUsecase({
    this.calculatorRepository,
  });

  Future<void> storeHistoryToLocal(CalculatorHistoryEntity entity) =>
      calculatorRepository.storeHistoryToLocal(entity);

  Future<List<CalculatorHistoryEntity>> getAllHistories() =>
      calculatorRepository.getAllHistories();

  Future<void> deleteHistory(String key) =>
      calculatorRepository.deleteHistory(key);

  Future<void> cleanAllHistory() => calculatorRepository.cleanAllHistory();
}
