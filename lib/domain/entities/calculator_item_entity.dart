import 'package:flutter/material.dart';

class CalculatorEntity {
  final Key key;
  final String title;
  final int value;
  final Operator operation;
  final Color bgColor;
  final Color color;
  final int flex;
  final IconData icon;

  CalculatorEntity({
    this.key,
    this.title,
    this.value,
    this.operation,
    this.bgColor,
    this.color,
    this.flex = 1,
    this.icon,
  })  : assert(value == null || operation == null, 'both cant be assigned'),
        assert(title == null || icon == null, 'both cant be assigned');
}

enum Operator {
  addition,
  substract,
  multiply,
  division,
  result,
  erase,
  clean,
}

class OperatorString {
  static const String addition = 'addition';
  static const String substract = 'substract';
  static const String multiply = 'multiply';
  static const String division = 'division';
  static const String result = 'result';
  static const String erase = 'erase';
  static const String clean = 'clean';
}

extension StringOperatorExpension on String {
  Operator toOperation() {
    switch (this) {
      case OperatorString.substract:
        return Operator.substract;
      case OperatorString.multiply:
        return Operator.multiply;
      case OperatorString.division:
        return Operator.division;
      case OperatorString.result:
        return Operator.result;
      case OperatorString.erase:
        return Operator.erase;
      case OperatorString.clean:
        return Operator.clean;
      case OperatorString.addition:
      default:
        return Operator.addition;
    }
  }
}

extension OperatorStringExtension on Operator {
  String get text {
    switch (this) {
      case Operator.addition:
        return OperatorString.addition;
      case Operator.substract:
        return OperatorString.substract;
      case Operator.multiply:
        return OperatorString.multiply;
      case Operator.division:
        return OperatorString.division;
      case Operator.result:
        return OperatorString.result;
      case Operator.erase:
        return OperatorString.erase;
      case Operator.clean:
        return OperatorString.clean;
      default:
        return '';
    }
  }
}
