import 'package:simple_calculator/domain/entities/calculator_history_entity.dart';

abstract class CalculatorRepository {
  Future<List<CalculatorHistoryEntity>> getAllHistories();
  Future<void> storeHistoryToLocal(CalculatorHistoryEntity entity);
  Future<void> deleteHistory(String key);
  Future<void> cleanAllHistory();
}
