import 'package:mockito/mockito.dart';
import 'package:simple_calculator/domain/repositories/calculator_repository.dart';

class MockCalculatorRepository extends Mock implements CalculatorRepository {}