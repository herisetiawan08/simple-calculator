import 'package:simple_calculator/data/datasource/local/calculator_history_local_datasource.dart';
import 'package:simple_calculator/domain/entities/calculator_history_entity.dart';
import 'package:simple_calculator/domain/repositories/calculator_repository.dart';

class CalculatorRepositoryImpl extends CalculatorRepository {
  CalculatorHistoryLocalDataSource calculatorHistoryLocalDataSource;
  CalculatorRepositoryImpl({this.calculatorHistoryLocalDataSource});

  @override
  Future<List<CalculatorHistoryEntity>> getAllHistories() =>
      calculatorHistoryLocalDataSource.getAll();

  @override
  Future<void> storeHistoryToLocal(CalculatorHistoryEntity entity) =>
      calculatorHistoryLocalDataSource.put(entity.timestamp, entity);

  @override
  Future<void> deleteHistory(String key) =>
      calculatorHistoryLocalDataSource.detele(key);

  @override
  Future<void> cleanAllHistory() =>
      calculatorHistoryLocalDataSource.deteleAll();
}
