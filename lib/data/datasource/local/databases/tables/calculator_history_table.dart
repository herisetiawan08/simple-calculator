import 'package:hive/hive.dart';
import 'package:simple_calculator/common/constants/hive_typeid_constants.dart';
import 'package:simple_calculator/data/models/calculator_history_model.dart';

part 'calculator_history_table.g.dart';

@HiveType(typeId: HiveTYpeIdConstants.calculatorHistory)
class CalculatorHistoryTable extends CalculatorHistoryModel {
  CalculatorHistoryTable({
    String firstNumber,
    String secondNumber,
    String operation,
    String result,
    String timestamp,
  }) : super(
          firstNumber: firstNumber,
          secondNumber: secondNumber,
          operation: operation,
          result: result,
          timestamp: timestamp,
        );

  factory CalculatorHistoryTable.fromModel(CalculatorHistoryModel model) =>
      CalculatorHistoryTable(
        firstNumber: model.firstNumber,
        secondNumber: model.secondNumber,
        operation: model.operation,
        result: model.result,
        timestamp: model.timestamp,
      );
}
