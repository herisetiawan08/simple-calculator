// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'calculator_history_table.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CalculatorHistoryTableAdapter
    extends TypeAdapter<CalculatorHistoryTable> {
  @override
  final int typeId = 93;

  @override
  CalculatorHistoryTable read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CalculatorHistoryTable()
      ..firstNumber = fields[0] as String
      ..secondNumber = fields[1] as String
      ..operation = fields[2] as String
      ..result = fields[3] as String
      ..timestamp = fields[4] as String;
  }

  @override
  void write(BinaryWriter writer, CalculatorHistoryTable obj) {
    writer
      ..writeByte(5)
      ..writeByte(0)
      ..write(obj.firstNumber)
      ..writeByte(1)
      ..write(obj.secondNumber)
      ..writeByte(2)
      ..write(obj.operation)
      ..writeByte(3)
      ..write(obj.result)
      ..writeByte(4)
      ..write(obj.timestamp);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CalculatorHistoryTableAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
