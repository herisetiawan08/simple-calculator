import 'package:mockito/mockito.dart';
import 'package:simple_calculator/data/datasource/local/calculator_history_local_datasource.dart';

class MockCalculatorHistoryLocalDataSource extends Mock
    implements CalculatorHistoryLocalDataSource {}
