import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';

class DatabaseUtil {
  static Future<void> initDatabase() async {
    final Directory directory = await getApplicationDocumentsDirectory();
    Hive.init(directory.path);
  }

  static void registerAdapter<Type>(TypeAdapter<Type> adapter) {
    Hive.registerAdapter(adapter);
  }

  static Future<Box<Type>> getBox<Type>({@required boxName}) async =>
      Hive.openBox(boxName);
}
