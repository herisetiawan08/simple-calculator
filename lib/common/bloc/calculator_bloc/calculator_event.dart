import 'package:simple_calculator/domain/entities/calculator_item_entity.dart';

abstract class CalculatorEvent {}

class CalculatorAddNumberEvent extends CalculatorEvent {
  final int value;

  CalculatorAddNumberEvent(this.value);

  @override
  int get hashCode => super.hashCode;

  @override
  bool operator ==(Object obj) {
    if (obj is CalculatorAddNumberEvent) {
      return obj.value == this.value;
    }
    return false;
  }
}

class CalculatorOperateEvent extends CalculatorEvent {
  final Operator operation;

  CalculatorOperateEvent(this.operation);
}

class CalculatorCountEvent extends CalculatorEvent {
  final void Function() callback;
  CalculatorCountEvent([this.callback]);
}

class CalculatorEraseEvent extends CalculatorEvent {}

class CalculatorCleanEvent extends CalculatorEvent {}
