import 'package:simple_calculator/domain/entities/calculator_item_entity.dart';

abstract class CalculatorState {
  final String prevResult;
  final Operator prevOperation;
  final String result;

  CalculatorState({
    this.prevResult,
    this.result,
    this.prevOperation,
  });
}

class CalculatorInitState extends CalculatorState {
  CalculatorInitState()
      : super(
          prevResult: '',
          result: '0',
        );
}

class CalculatorResultState extends CalculatorState {
  CalculatorResultState({
    String prevResult,
    String result,
    Operator prevOperation,
  }) : super(
          prevResult: prevResult,
          result: result,
          prevOperation: prevOperation,
        );
}
